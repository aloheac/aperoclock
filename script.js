/*
 * Brief: Given an array, return a random item
 *
 * Return one element of the array
 */
function getRandomItem(array) {
    var arrayLength = array.length;
    var result;
    var idx;

    if (arrayLength == 1) {
        result = array[0];

    } else {
        idx = Math.floor((Math.random() * arrayLength));
        result = array[idx];
    }

    return result;
}

/*
 * Brief: Given the date, say if it's time to drink or not
 *
 * Returns {answer, advice}
 *    The answer and the advice is a human readable message
 *    The answer says if you should be drinking
 *    The advice says, whatever the answer is, if you could drink
 */
function getCurrentMessage(hours, minutes) {
    var advice = "";
    var answer;

    var allAnswers = {
        0: "Il n'est pas l'heure de l'apéro",
        1: "Ce n'est plus de l'apéro qu'il s'agit, mais c'est le même principe !",
        3: "Il est l'heure de l'apéro",
        4: "Il est presque l'heure de l'apéro",
        5: "Il n'est pas l'heure de l'apéro, mais place au dodo",
        6: "Il n'est plus l'heure de l'apéro, mais place au dijo",
        7: "Toujours là ?! Chapeau ;)",
        8: "C'est long, c'est long, c'est long... Il est des nooooootre !!!"
    };

    var allAdvices = {
        anisette: "d'une anisette",
        baileys: "du Baileys",
        demi: "du demi",
        get: "du Get",
        martini: "du Martini",
        pastis: "du pastis",
        ricard: "du Ricard",
        spiritueux: "d'un spiritueux",
        suze: "d'une suze",
        vin: "du vin",
        whisky: "du whisky"
    };

    var answerHourly = {
        0: allAnswers[8],
        1: allAnswers[8],
        2: allAnswers[8],
        3: allAnswers[8],
        4: allAnswers[8],
        5: allAnswers[7],
        6: allAnswers[7],
        7: allAnswers[0],
        8: allAnswers[0],
        9: allAnswers[0],
        10: allAnswers[0],
        11: allAnswers[3],
        12: allAnswers[3],
        13: allAnswers[6],
        14: allAnswers[5],
        15: allAnswers[0],
        16: allAnswers[0],
        17: allAnswers[4],
        18: allAnswers[3],
        19: allAnswers[3],
        20: allAnswers[3],
        21: allAnswers[1],
        22: allAnswers[1],
        23: allAnswers[1]
    };

    var adviceMinutely = {
        6: [allAdvices.pastis],
        7: [allAdvices.anisette],
        10: [allAdvices.pastis],
        16: [allAdvices.baileys],
        17: [allAdvices.anisette],
        20: [allAdvices.vin],
        26: [allAdvices.pastis],
        27: [allAdvices.get, allAdvices.anisette],
        30: [allAdvices.demi, allAdvices.martini, allAdvices.whisky],
        31: [allAdvices.get],
        36: [allAdvices.pastis],
        37: [allAdvices.anisette],
        40: [allAdvices.vin],
        45: [allAdvices.ricard],
        46: [allAdvices.pastis],
        47: [allAdvices.anisette],
        50: [allAdvices.pastis],
        56: [allAdvices.pastis],
        53: [allAdvices.anisette],
        57: [allAdvices.anisette],
        58: [allAdvices.spiritueux]
    }

    answer = answerHourly[hours];

    if ( ! adviceMinutely[minutes] ) {
        advice = "";
    } else {
        advice = getRandomItem(adviceMinutely[minutes]);
        hours = ("0" + hours).slice(-2);
        minutes = ("0" + minutes).slice(-2);
        advice = "Petite suggestion : il est " + hours + "h" + minutes + ", l'heure " + advice;
    }

    return {Answer: answer, Advice: advice};
}

/*
 * Brief: Display the given time
 */
function updateTimeView(date) {
    var text;
    var options = {
        hour: "2-digit",
        minute: "2-digit"
    };

    text = date.toLocaleTimeString("fr-FR", options);

    document.getElementById("time").innerHTML = text;
}

/*
 * Brief: Display the given date
 */
function updateDateView(date) {
    var text;
    var options = {
        weekday: "long",
        day: "2-digit",
        month: "long",
        year: "numeric"
    };

    text = date.toLocaleDateString("fr-FR", options);

    document.getElementById("date").innerHTML = text;
}

/*
 * Brief: Update the page with a message specific to the current date and make
 * sure it's updated the next minute
 */
function updateMessage() {
    var today = new Date();
    var hours = today.getHours();
    var minutes = today.getMinutes();
    var seconds = today.getSeconds();
    var msg = {};
    var timeout = 0;

    console.debug("Update message at " + today.toString());

    msg = getCurrentMessage(hours, minutes);

    updateTimeView(today);
    updateDateView(today);
    document.getElementById("answer").innerHTML = msg.Answer;
    document.getElementById("advice").innerHTML = msg.Advice;

    // Now wait for the next minute
    timeout = 60 - seconds;
    timeout += 1; // I am not a swiss
    setTimeout(updateMessage, timeout*1000);
}

/*
 * Brief: For test purposes, givent a date and an answer, create an text to be
 * displayed
 */
function testAppendNewElement(hour, minute, advice) {
    var newElement;
    var adviceElement = document.getElementById("advice");
    var text = hour + "h" + minute + ": " + advice;

    newElement = document.createElement('div');
    newElement.className += "one-advice";
    newElement.innerHTML = text;
    adviceElement.appendChild(newElement);
}

/*
 * Brief: For test purposes, list messages for every minutes
 */
function testMessages() {
    var hour = 0;
    var minute = 0;

    for (hour=0; hour<24; hour+=1) {
        for (minute=0; minute<60; minute+=1) {
            msg = getCurrentMessage(hour, minute);
            testAppendNewElement(hour, minute, msg.Advice);
        }
    }
}

function documentLoaded() {
    var url = new URL(window.location.href);
    var testValue = url.searchParams.get("test");

    if (testValue== "true") {
        console.info("Test request");
        testMessages();
    } else {
        updateMessage();
    }
}

window.onload = documentLoaded;
